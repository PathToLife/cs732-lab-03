import { v4 as uuid } from 'uuid';
import dayjs from 'dayjs';
import { articles } from './random-articles';

function createArticle(article) {
    const newArticle = {
        ...article,
        id: uuid(),
        date: dayjs().format()
    };
    articles.push(newArticle);
    return newArticle;
}

function retrieveArticleList() {
    return articles;
}

function retrieveArticle(id) {
    return articles.find(a => a.id === id);
}

function updateArticle(article) {

    const index = articles.findIndex(a => a.id === article.id);

    if (index >= 0) {
        articles[index] = { ...articles[index], ...article };
        return true;
    }
    return false;
}

function deleteArticle(id) {
    articles = articles.filter(a => a.id !== id);
}

export {
    createArticle,
    retrieveArticle,
    retrieveArticleList,
    updateArticle,
    deleteArticle
}