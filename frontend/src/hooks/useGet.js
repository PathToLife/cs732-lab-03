import { useEffect, useState } from 'react'
import axios from 'axios'

const fetchData = async (url) => {
    const response = await axios.get(url)
    return response.data
}

/**
 * A custom hook which fetches data from the given URL. Includes functionality to determine
 * whether the data is still being loaded or not.
 */
export default function useGet(url, initialState = null) {
    const [data, setData] = useState(initialState)
    const [isLoading, setLoading] = useState(false)

    const reFetch = () => {
        setLoading(true)
        fetchData(url)
            .then((data) => {
                setData(data)
            })
            .finally(() => {
                setLoading(false)
            })
    }

    useEffect(() => {
        setLoading(true)
        fetchData(url)
            .then((data) => {
                setData(data)
            })
            .finally(() => {
                setLoading(false)
            })
    }, [url])

    return { data, isLoading, reFetch }
}
